package com.example.bottomnavigation.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import com.example.bottomnavigation.R

class PlayerFragment: Fragment(R.layout.fragment_player) {

    private lateinit var runnable: Runnable
    private var handler = Handler()

    private lateinit var mediaPlayer: MediaPlayer

    private lateinit var playButton: ImageView
    private lateinit var seekBar: SeekBar

    private lateinit var nextButton: ImageView
    private lateinit var previousButton: ImageView

    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        seekBar = view.findViewById(R.id.musicProgress)

        /* SongsFragment-ში რომელ მუსიკასაც დააწვება იმ მუსიკის ID-ს დაუკრავს */

        var idSong: Int = PlayerFragmentArgs.fromBundle(requireArguments()).songId

        if (idSong == 0) {
            idSong = R.raw.first
        }

        mediaPlayer = MediaPlayer.create(activity, idSong)

        seekBar.progress = 0

        seekBar.max = mediaPlayer.duration

        playButton = view.findViewById(R.id.playButton)
        playButton.setOnClickListener {
            if (!mediaPlayer.isPlaying) {

                val sharedPreferences = activity?.getSharedPreferences("mediaPlayer", Context.MODE_PRIVATE)

                val speed = sharedPreferences?.getFloat("speed", 1f) // დაკვრის სიჩქარისთვის

                val params = mediaPlayer.playbackParams

                params.speed = speed!!

                mediaPlayer.playbackParams = params

                mediaPlayer.start()
                playButton.setImageResource(R.drawable.ic_baseline_pause_24)
            } else {
                mediaPlayer.pause()
                playButton.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            }
        }

        nextButton = view.findViewById(R.id.nextButton)
        nextButton.setOnClickListener {
            mediaPlayer.seekTo(mediaPlayer.currentPosition + 10000)
        }

        previousButton = view.findViewById(R.id.previousButton)
        previousButton.setOnClickListener {
            mediaPlayer.seekTo(mediaPlayer.currentPosition - 10000)
        }

        seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if(p2) {
                    mediaPlayer.seekTo(p1)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        })

        runnable = Runnable {
            seekBar.progress = mediaPlayer.currentPosition
            handler.postDelayed(runnable, 1000)
        }
        handler.postDelayed(runnable, 1000)

        mediaPlayer.setOnCompletionListener {
            playButton.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            seekBar.progress = 0
        }

    }

    override fun onStop() {
        super.onStop()
        mediaPlayer.pause()
        playButton.setImageResource(R.drawable.ic_baseline_play_arrow_24)
    }

}