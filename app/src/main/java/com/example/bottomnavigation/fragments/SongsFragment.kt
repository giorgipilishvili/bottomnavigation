package com.example.bottomnavigation.fragments

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.bottomnavigation.R

class SongsFragment: Fragment(R.layout.fragment_songs) {

    private lateinit var firstSong: LinearLayout
    private lateinit var songName: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        songName = view.findViewById(R.id.songName)

        firstSong = view.findViewById(R.id.firstSong)
        firstSong.setOnClickListener {

            val songId: Int = R.raw.first

            val controller = Navigation.findNavController(view)

            val action = SongsFragmentDirections.actionSongsFragmentToPlayerFragment(songId)
            controller.navigate(action)

        }

    }

}