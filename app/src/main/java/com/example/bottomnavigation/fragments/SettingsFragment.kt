package com.example.bottomnavigation.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.bottomnavigation.R
import com.google.android.material.button.MaterialButtonToggleGroup

class SettingsFragment: Fragment(R.layout.fragment_settings) {

    private lateinit var themeButton: MaterialButtonToggleGroup

    private lateinit var speedButtons: MaterialButtonToggleGroup

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        themeButton = view.findViewById(R.id.themeButtons)

        val sharedPreferences = activity?.getSharedPreferences("Theme", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()

        val thId = when (sharedPreferences?.getInt("themeMode", AppCompatDelegate.MODE_NIGHT_NO)) {
            AppCompatDelegate.MODE_NIGHT_YES -> {
                R.id.darkTheme
            }
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM -> {
                R.id.systemTheme
            }
            else -> {
                R.id.lightTheme
            }
        }

        themeButton.check(thId) /* რომელი თემაც აყენია ის მოინიშნოს პარამეტრებში */

        /* თემის შეცვლა */

        themeButton.addOnButtonCheckedListener(object : MaterialButtonToggleGroup.OnButtonCheckedListener {

            override fun onButtonChecked(
                group: MaterialButtonToggleGroup?,
                checkedId: Int,
                isChecked: Boolean
            ) {
                if (isChecked) {

                    val theme = when (checkedId) {
                        R.id.systemTheme -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                        R.id.darkTheme -> AppCompatDelegate.MODE_NIGHT_YES
                        else -> AppCompatDelegate.MODE_NIGHT_NO
                    }

                    AppCompatDelegate.setDefaultNightMode(theme)

                    editor?.apply {
                        putInt("themeMode", theme)
                    }?.apply()

                }
            }

        })

        /* დაკვრის სიჩქარის შეცვლა */

        speedButtons = view.findViewById(R.id.speedButtons)

        val sharedPreferences1 = activity?.getSharedPreferences("mediaPlayer", Context.MODE_PRIVATE)
        val editor1 = sharedPreferences1?.edit()

        val spId = when (sharedPreferences1?.getFloat("speed", 1f)) {
            1f -> {
                R.id.normal
            }
            1.5f -> {
                R.id.fast
            }
            else -> {
                R.id.slow
            }
        }

        speedButtons.check(spId) /* რომელი სისწრაფეც აყენია ის მოინიშნოს პარამეტრებში */

        speedButtons.addOnButtonCheckedListener(object : MaterialButtonToggleGroup.OnButtonCheckedListener {

            override fun onButtonChecked(
                group: MaterialButtonToggleGroup?,
                checkedId: Int,
                isChecked: Boolean
            ) {
                if (isChecked) {

                    val speed = when (checkedId) {
                        R.id.slow -> 0.75f
                        R.id.fast -> 1.5f
                        else -> 1f
                    }

                    editor1?.apply {
                        putFloat("speed", speed)
                    }?.apply()

                }
            }

        })

    }

}