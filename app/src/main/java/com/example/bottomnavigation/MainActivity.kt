package com.example.bottomnavigation

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* ის თემა დაყენდეს რომელიც sharedPreferences-შია შენახული. default-ად light-ი */

        val sharedPreferences = getSharedPreferences("Theme", Context.MODE_PRIVATE)

        val theme = sharedPreferences.getInt("themeMode", AppCompatDelegate.MODE_NIGHT_NO)

        AppCompatDelegate.setDefaultNightMode(theme)

        /*  */

        supportActionBar?.hide() // actionBar დავმალე აქ

        val bottomNavView = findViewById<BottomNavigationView>(R.id.bottomNavView)

        val controller = findNavController(R.id.nav_host_fragment)

        val appBarConfig = AppBarConfiguration(setOf(
            R.id.songsFragment,
            R.id.playerFragment,
            R.id.favoritesFragment,
            R.id.settingsFragment
        ))

        setupActionBarWithNavController(controller, appBarConfig)
        bottomNavView.setupWithNavController(controller)

    }

}